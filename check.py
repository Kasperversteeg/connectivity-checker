# Simple site connectivity checker
# It needs to do the following:
# - Create loop that repeats every x seconds
# - Read json file and iterate over items
# - Connect to the website
# - Check if there is a response
# - If response is ok echo it
# - If response is not ok
#   - Create function that checks  every 5 seconds
#   - Check every 5 seconds if something changed
#   - If anything changed, do nothing and continue
#   - If nothing changed, send e-mail

import urllib.request
import time
from website import Website

seconds = 1

def loop_for_seconds(seconds):
    while True:
        check_connectivity(reserv_app.url)
        time.sleep(seconds)

def make_website(name, url):
    website = Website(name, url)
    return website

def connect(host):
    try:
        urllib.request.urlopen(host) #Python 3.x
        return True
    except:
        return False

def check_connectivity(url):
    if connect(url):
      print("%s is running!" % (url))
    else:
      print("%s not running!" % (url))

# Create object per website to check
reserv_app = make_website('ReserV-app', 'https://reserv-app.nl')
reserv_app.start_connectivity_check()