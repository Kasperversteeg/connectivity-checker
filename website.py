import urllib.request
import time

class Website(object):
    url = ""
    name = ""
    seconds = 1

    # The class "constructor" - It's actually an initializer
    def __init__(self, name, url):
        self.url = url
        self.name = name

    def test_connection(self):
        try:
            urllib.request.urlopen(self.url) #Python 3.x
            print("%s is running!" % (self.url))
            return True
        except:
            print("%s is running!" % (self.url))
            return False

    def start_connectivity_check(self):
        while True:
            self.test_connection()
            time.sleep(self.seconds)

